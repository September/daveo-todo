export default class ListTask {
  constructor(title, tasks) {
    this.title = title;
    this.tasks = tasks;
  }
}
