import Vue from 'vue';
import Vuex from 'vuex';
import uuid from 'uuid';
import Task from './entities/Task';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    tasks: [],
  },
  mutations: {
    ADD_TASK: (state, text) => {
      if (text !== '') {
        return state.tasks.push(new Task(uuid.v4(), text, 0));
      }
      return state;
    },
    DEL_TASK: (state, obj) => {
      state.tasks.splice(state.tasks.indexOf(obj), 1);
    },
    INCREMENTE_STATUS: (state, obj) => {
      return state.tasks.map((e) => {
        if (e.id === obj.id) {
          e.status += 1;
        }
        return e;
      });
    },
  },
  actions: {
    addTask: (store, text) => {
      store.commit('ADD_TASK', text);
    },
    deleteTask: (store, object) => {
      store.commit('DEL_TASK', object);
    },
    incrementeStatus: (store, object) => {
      store.commit('INCREMENTE_STATUS', object);
    },
  },
  getters: {
    tasks: state => state.tasks,
    tasksToDo: state => state.tasks.filter(e => e.status === 0),
    tasksDoing: state => state.tasks.filter(e => e.status === 1),
    tasksDone: state => state.tasks.filter(e => e.status === 2),
  },
});
